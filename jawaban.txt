1. Membuat database
create database myshop;

2. Membuat Table
User :
MariaDB [myshop]> create table user(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255),
    -> primary key(id)
    -> );


category :
MariaDB [myshop]> create table categ(
	-> id int(8) auto_increment,
	-> nama varchar(30),
	-> primary key(id)
	-> );

Melihat Isi dari Tabel : describe categ;

Membuat tabel items
MariaDB [myshop]> create table items(
    -> id int primary key auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int,
    -> stock int,
    -> categ_id int,
    -> foreign key (categ_id) references categ(id)
    -> );
3. Memasukkan Data pada Table:

MariaDB [myshop]> insert into categ(nama) value("gadged"),("cloth"),("men"),("women"),("branded");
MariaDB [myshop]> insert into items(name,description, price, stock, categ_id) values ("sumsang b50","hape keren dari sumsang", "4000000","100",1),("Uniklooh"," baju keren dari brand ternama", "500000","50",2),("IMHO watch", " jam tangan anak yang paling jujur banget", "2000000","10",1);
MariaDB [myshop]> insert into user(name,email,password) value("John Doe","john@doe.com","john123"),("Jane Doe","jane@doe.com","jenita123");

4.  
a. select name, email from user;
b.1. select *from items where price >1000000;
b.2. select * from items where categ_id like '1%';
c. MariaDB [myshop]> select items.name,items.description,items. price,items.stock,items.categ_id, categ.nama from items
    -> inner join categ on items.categ_id =categ.id;

5. MariaDB [myshop]> update items set price = "2500000" where id = 1;